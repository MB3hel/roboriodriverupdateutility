# RoboRIO Driver Update Utility

The roboRIO imaging tool released with FRC2019's kickoff release has a bug. HAL drivers are not updated when the roboRIO is reimaged. This prevents sensors using SPI and other devices from working properly with the FRC2019 kickoff image. This utility uses SSH to run the updateNIDrivers command which will update the drivers as needed. After running this utility everything should work as intended.

## Downloads
TODO

## Building
- Install [CMake](https://cmake.org/) and [conan](https://conan.io/)
- Install Visual Studio 2017 with C++ support (Desktop)
- Run the following commands in the project directory
```
mkdir build
cd build
conan install ..
cmake .. -G "Visual Studio 15 2017 Win64"
cmake --build
```

Resulting executable will be in bin directory.