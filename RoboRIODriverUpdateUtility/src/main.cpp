#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
# include <winsock2.h>
#else
# include <sys/socket.h>
#define SOCKET int
#endif

#include <iostream>
#include <ios>
#include <string>
#include <sstream>
#include <cctype>
#include <limits>
#include <libssh2.h>

#ifdef _WIN32
#include <windows.h>
/**
 * Get number of columns in console window
 */
int getConsoleWidth(){
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    return csbi.srWindow.Right - csbi.srWindow.Left + 1;
}
#else
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
/**
 * Get number of columns in console window
 */
int getConsoleWitdh(){
    winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return w.ws_col;
}
#endif

std::string errmsg(std::string message){
    std::stringstream msg;
    msg << "******" << std::endl << "******ERROR: " << message << std::endl << "******";
    return msg.str();
}

std::string errmsg(std::string message, int rc){
    std::stringstream msg;
    msg << "******" << std::endl << "******ERROR: " << message << "Error code " << rc << "." << std::endl << "******";
    return msg.str();
}

/* Adapted from http://www.cplusplus.com/forum/beginner/132223/
 * This function takes a string and an output buffer and a desired width. It then copies
 * the string to the buffer, inserting a new line character when a certain line
 * length is reached.  If the end of the line is in the middle of a word, it will
 * backtrack along the string until white space is found.
 */
std::string word_wrap(std::string text, unsigned per_line){
    unsigned line_begin = 0;

    while (line_begin < text.size()){
        const unsigned ideal_end = line_begin + per_line ;
        unsigned line_end = ideal_end <= text.size() ? ideal_end : text.size()-1;

        if (line_end == text.size() - 1)
            ++line_end;
        else if (std::isspace(text[line_end])){
            text[line_end] = '\n';
            ++line_end;
        }
        else    // backtrack
        {
            unsigned end = line_end;
            while ( end > line_begin && !std::isspace(text[end]))
                --end;

            if (end != line_begin)                  
            {                                       
                line_end = end;                     
                text[line_end++] = '\n';            
            }                                       
            else                                    
                text.insert(line_end++, 1, '\n');
        }

        line_begin = line_end;
    }

    return text;
}

std::string word_wrap(std::string text){
    return word_wrap(text, getConsoleWidth() - 1);
}

/**
 * Close the channel, session, socket then shutdown libssh2
 * @param session Pointer to session to close
 * @param sock Pointer to socket to close
 */
void shutdown(LIBSSH2_CHANNEL *channel, LIBSSH2_SESSION *session, SOCKET *sock){
    if(channel != NULL)
        libssh2_channel_free(channel);
    libssh2_session_disconnect(session, "Normal Shutdown, Thank you for playing");
    libssh2_session_free(session);

#ifdef _WIN32
    closesocket(*sock);
#else
    close(sock);
#endif

    libssh2_exit();
}


bool doMain(){

    // Connection info
    const char *hostname = "172.22.11.2";
    const int port = 22;
    const char *username = "admin";
    const char *password = "";
    
    // Return code
    int rc = 0;

    // LibSSH2
    LIBSSH2_SESSION *session;
    LIBSSH2_CHANNEL *channel = NULL;
    const char *fingerprint;

    // Socket connection
    SOCKET sock;
    sockaddr_in sin;
    unsigned int hostaddr;

    // Print welcome messate
    std::string title = "RoboRIO Driver Update Utility v1.0.6 (c) 2019 Marcus Behel";
    std::string message = "The roboRIO imaging tool released with FRC2019's kickoff release has a bug. HAL drivers are not updated when the roboRIO is reimaged. This prevents sensors using SPI and other devices from working properly with the FRC2019 kickoff image. This utility uses SSH to run the updateNIDrivers command which will update the drivers as needed. After running this utility everything should work as intended.";

    std::cout << word_wrap(title) << std::endl
              << std::endl
              << word_wrap(message) << std::endl << std::endl;
    std::cout << word_wrap("Connect the roboRIO to this PC by USB. Make sure the roboRIO is powered on.") << std::endl
              << word_wrap("When ready press enter to start the process.") << std::endl;
	
    std::cin.ignore(); // Wait for enter
    std::cout << word_wrap("Connecting to roboRIO...") << std::endl;

    // Setup Winsock
#ifdef WIN32
    WSADATA wsadata;
    int err;
 
    err = WSAStartup(MAKEWORD(2,0), &wsadata);
    if (err != 0) {
        std::cerr << word_wrap(errmsg("Failed to setup winsock.")) << std::endl;
        return false;
    }
#endif

    // Initialize LibSSH2
    rc = libssh2_init(0);
    if(rc != 0){
        std::cerr << word_wrap(errmsg("Failed to initialize libssh2.", rc)) << std::endl;
        return false;
    }

    // Open a socket and initialize an SSH session
    hostaddr = inet_addr(hostname);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr = hostaddr;
    if((rc = connect(sock, (sockaddr*)(&sin), sizeof(sockaddr_in))) != 0){
        std::cerr << word_wrap(errmsg("Failed to open socket.", rc)) << std::endl;
        return false;
    }

    // Initialize session
    session = libssh2_session_init();
    if(libssh2_session_handshake(session, sock)){
        std::cerr << word_wrap(errmsg("Failed to establish SSH session.")) << std::endl;
        return false;
    }
    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);

    // Authenticate
    if (libssh2_userauth_password(session, username, password) != 0) {
        std::cerr << word_wrap(errmsg("Failed to authenticate as user admin. Make sure there is no password set.")) << std::endl;
        shutdown(NULL, session, &sock);
        return false;
    }

    // Open shell session
    if (!(channel = libssh2_channel_open_session(session))) {
        std::cerr << word_wrap(errmsg("Failed to open SSH channel.")) << std::endl;
        shutdown(NULL, session, &sock);
        return false;
    }
    if (libssh2_channel_request_pty(channel, "vanilla")) {
        std::cerr << word_wrap(errmsg("Failed to request pty.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }
     if (libssh2_channel_shell(channel)) {
        std::cerr << word_wrap(errmsg("Failed to open shell on requested pty.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }

    std::cout << word_wrap("******WARNING: STARTING. DO NOT UNPLUG THE RIO, DISCONNECT THE RIO FROM THIS PC OR, CLOSE THIS WINDOW!!!") << std::endl << std::endl;

    Sleep(1000);

    size_t count = 0;
    const int len = 4000;
    char *buffer = new char[len]();

    // Print system info
    const char *unameCmd = "uname -a\n";
    if(libssh2_channel_write(channel, unameCmd, strlen(unameCmd)) < 0){
        std::cerr << word_wrap(errmsg("Failed to write to SSH session.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }

    // Make sure enough memory is free to build kernel modules
    const char *killCommand = "frcKillRobot.sh -t\n";
    if(libssh2_channel_write(channel, killCommand, strlen(killCommand)) < 0){
        std::cerr << word_wrap(errmsg("Failed to write to SSH session.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }

    // Send command. Wait for reboot message
    const char *cmd = "updateNIDrivers\n";
    if(libssh2_channel_write(channel, cmd, strlen(cmd)) < 0){
        std::cerr << word_wrap(errmsg("Failed to execute update command.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }

    while(true){
        count = libssh2_channel_read(channel, buffer, len);
        if(count > 0){
            for(size_t i = 0; i < count; ++i)
                std::cout << buffer[i];
        }else if(count < 0){
            std::cerr << word_wrap(errmsg("Failed to readback from roboRIO.")) << std::endl;
            shutdown(channel, session, &sock);
            return false;
        }
        if(strstr(buffer, "Would you like to reboot now? [yes|no]"))
            break;
        if(strstr(buffer, "ERROR: Update of National Instruments drivers failed.")){
            shutdown(channel, session, &sock);
            return false;
        }
    }
    
    // Respond to reboot message
    const char *response = "yes\n";
    if(libssh2_channel_write(channel, response, strlen(response)) < 0){
        std::cerr << word_wrap(errmsg("Failed to write to SSH session.")) << std::endl;
        shutdown(channel, session, &sock);
        return false;
    }
    count = libssh2_channel_read(channel, buffer, len);
    for(size_t i = 0; i < count; ++i)
            std::cout << buffer[i];

    // Close channel
    delete buffer;
    libssh2_channel_send_eof(channel);
    libssh2_channel_close(channel);
    libssh2_channel_free(channel);

    // Close everything (channel is closed above. Do not free again!!!)
    shutdown(NULL, session, &sock);

    std::cout << std::endl << std::endl << word_wrap("Driver update completed successfully. roboRIO is rebooting.") << std::endl;
    std::cout << word_wrap("Press enter to exit.") << std::endl;
    std::cin.ignore();
    return true;
}

int main(){
    bool res = doMain();
    if(!res){
        std::cerr << std::endl << std::endl << word_wrap("Did not complete successfully! Press enter to exit.") << std::endl;
        std::cin.ignore();
    }
}